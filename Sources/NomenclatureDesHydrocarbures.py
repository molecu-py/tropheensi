#importation des bibliothèques de QT Designer
from PyQt5.uic import loadUi #interface
from PyQt5.QtWidgets import QApplication 
from PyQt5.QtGui import QPixmap #image

reponse_q1=""
reponse_q2=""
reponse_q3=""
reponse_q4=""
reponse_q5=""
reponse_q6=""

style_bouton = """
    QPushButton {
        padding: 5px 5px;
        background-color: #4e7cff;
        color: #fff;
        text-decoration: none;
        border: none;
        border-radius: 5px;
        font-size: 15px;
        font-weight: bold;
        text-align: center;
    }

    QPushButton:hover {
        background-color: #2a4a7d;
    }
"""

style_question = """
        QLabel {
        font-size: 18px;
        color: #333; 
        font-weight: bold;
        margin-bottom: 10px;
    }
    """

style_titre = """
        QLabel {
        font-size: 48px;
        font-weight: bold;
        text-transform: uppercase;
        color: #4e7cff;
        margin-bottom: 30px;
    }
    """

style_texte_intro= """
        QLabel {
        font-family: Arial, sans-serif; 
        font-size: 26px; 
        color: #333;
        line-height: 1.5; 
        text-align: justify;
        margin-bottom: 20px; 
    }
    """


def generer_resultat():
    """
    Fonction appelée "generer_resultat": génère le résultat en fonction des réponses aux questions précédentes.

    Utilise les variables globales suivantes :
    - reponse_q1
    - reponse_q2
    - reponse_q3
    - reponse_q4
    - reponse_q5
    - reponse_q6

    Voici deux exemples de comment elle fonctionne :
    - Si reponse_q1 est "1C" et reponse_q2 est "NON", le résultat est "Méthane".
    - Si reponse_q1 est "1C", reponse_q2 est "OUI" et reponse_q3 est "Alcool", le résultat est "Méthanol".

    -Met à jour l'étiquette `resultat_label` avec le résultat et affiche l'image correspondante (la carte d'itentité de la molecule).
    
    """
    global reponse_q1
    global reponse_q2
    global reponse_q3
    global reponse_q4
    global reponse_q5
    global reponse_q6
    
    resultat=""
    
    if reponse_q1=="1C" and reponse_q2=="NON":
        resultat="Méthane"
        
    elif reponse_q1=="2C" and reponse_q2=="NON":
        resultat="Ethane"
        
        
    elif reponse_q1=="1C" and reponse_q2=="OUI" and reponse_q3=="Alcool":
        resultat="Methanol"
    elif reponse_q1=="1C" and reponse_q2=="OUI" and reponse_q3=="Aldehyde":
        resultat="Methanal"
    elif reponse_q1=="1C" and reponse_q2=="OUI" and reponse_q3=="Acide":
        resultat="Acide méthanoique"
        
        
    elif reponse_q1=="2C" and reponse_q2=="OUI" and reponse_q3=="Alcool":
        resultat="Ethanol"
    elif reponse_q1=="2C" and reponse_q2=="OUI" and reponse_q3=="Aldehyde":
        resultat="Ethanal"
    elif reponse_q1=="2C" and reponse_q2=="OUI" and reponse_q3=="Acide":
        resultat="Acide Ethanoique"
        
        
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="NON":
        resultat="Propane"
    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="NON":
        resultat="Butane"
        
        
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C1":
        resultat="Propanol"
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C2":
        resultat="Propan-2-ol"
    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C1":
        resultat="Butanol"
    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C2":
        resultat="Butan-2-ol"
        
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Aldehyde":
        resultat="Propanal"
    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Aldehyde":
        resultat="Butanal"
    
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Cetone" and reponse_q5=="C2":
        resultat="Propan-2-one"

    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Cetone" and reponse_q5=="C2":
        resultat="Butan-2-one"
    
    elif reponse_q1=="3C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Acide":
        resultat="Acide propanoique"
    elif reponse_q1=="4C" and reponse_q2=="NON" and reponse_q3=="OUI" and reponse_q4=="Acide":
        resultat="Acide butanoique"
        
    
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C1":
        resultat="2-methylpropanol"
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C2":
        resultat="2-methylpropan-2-ol"
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Cetone":
        resultat="2-methylpropan-2-one"
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Aldehyde":
        resultat="2-methylpropanal"
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Acide":
        resultat="2-methylpropanoique"
        
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C1" and reponse_q6=="C2":
        resultat="2-methylbutanol"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C1" and reponse_q6=="C3":
        resultat="3-methylbutanol"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C2" and reponse_q6=="C2":
        resultat="2-methylbutan-2-ol"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Alcool" and reponse_q5=="C2" and reponse_q6=="C3":
        resultat="3-methylbutan-2-ol"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Cetone":
        resultat="3-methylbutan-2-one"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Aldehyde" and reponse_q6=="C2":
        resultat="2-methylbutanal"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Aldehyde" and reponse_q6=="C3":
        resultat="3-methylbutanal"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Acide" and reponse_q6=="C2":
        resultat="Acide 2-methylbutanoique"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="OUI" and reponse_q4=="Acide" and reponse_q6=="C3":
        resultat="Acide 3-methylbutanoique"
    
    
    elif reponse_q1=="3C" and reponse_q2=="OUI" and reponse_q3=="NON":
        resultat="2-méthylpropane"
    elif reponse_q1=="4C" and reponse_q2=="OUI" and reponse_q3=="NON":
        resultat="2-méthylbutane"
        
    resultat_label = windows.resultat
    resultat_label.setText(resultat) 
    pixmap = QPixmap("molecules/"+resultat+".png")
    pixmap = pixmap.scaled(resultat_label.size(), aspectRatioMode=True)
    resultat_label.setPixmap(pixmap)
    

    
def config_par_defaut():
    
    """
    Fonction appelée "config_par_defaut": configure les styles par défaut pour les éléments de l'interface utilisateur.

    Utilise les variables globales suivantes :
    - style_bouton
    - style_question
    - style_texte_intro
    - style_titre

    Fonctionnement: (3exemples)
    - Définit le style du bouton de démarrage ("Demarrer")(`b_start`) avec les propriétés spécifiées.
    - Définit le style du texte de démarrage (`label_demarrer`) avec les propriétés spécifiées.
    - Charge une image (`cours.png`)(dans le fichier "Molecules") et l'affiche dans l'étiquette `cours_label`.
    
    """
    global style_bouton
    global style_question
    global style_texte_intro
    
    style_boutton_intro ="""
        QPushButton {
            font-size: 24px;
            font-weight: bold;
            color: white;
            background-color: #4e7cff;
            border: 2px solid #3557d1;
            border-radius: 10px;
            padding: 20px;
         }
         QPushButton:hover {
            background-color: #3557d1;
         }
         QPushButton:pressed {
            background-color: #2a448a; 
         }
         
    """
    
# ------------ ---------- Configration esthethique  ----------------------
    #Start
    windows.b_start.setStyleSheet(style_boutton_intro)
    windows.label_demarrer.setStyleSheet(style_texte_intro)
    
    #cours
    pixmap = QPixmap("Molecules/cours.png") 
    pixmap = pixmap.scaled(windows.cours_label.size(), aspectRatioMode=True)
    windows.cours_label.setPixmap(pixmap)
    windows.b_try.setStyleSheet(style_boutton_intro)
    
    #Question 1
    windows.label.setStyleSheet(style_question)
    windows.label_2.setStyleSheet(style_titre)
    
    #bouton question 1
    windows.b_1c.setStyleSheet(style_bouton)
    windows.b_2c.setStyleSheet(style_bouton)
    windows.b_3c.setStyleSheet(style_bouton)
    windows.b_4c.setStyleSheet(style_bouton)
    
    #bouton question 2
    windows.b_oui.setStyleSheet(style_bouton)
    windows.b_non.setStyleSheet(style_bouton)
    
    #bouton question 3
    windows.b_oui_3.setStyleSheet(style_bouton)
    windows.b_non_3.setStyleSheet(style_bouton)
    
    windows.b_alcool.setStyleSheet(style_bouton)
    windows.b_aldehyde.setStyleSheet(style_bouton)
    windows.b_acide.setStyleSheet(style_bouton)
    
    #bouton question 4
    windows.b_alcool_4.setStyleSheet(style_bouton)
    windows.b_aldehyde_4.setStyleSheet(style_bouton)
    windows.b_cetone_4.setStyleSheet(style_bouton)
    windows.b_acide_4.setStyleSheet(style_bouton)
    
    #bouton question 5
    windows.b_cf1.setStyleSheet(style_bouton)
    windows.b_cf2.setStyleSheet(style_bouton)
    
    #bouton question 6
    windows.b_c2.setStyleSheet(style_bouton)
    windows.b_c3.setStyleSheet(style_bouton)

# ------------ ---------- Affichage des questions et des reponses  ----------------------
    #cours
    windows.cours_label.setVisible(False)

    #Question 1
    windows.label.setVisible(False)
    windows.b_1c.setVisible(False)
    windows.b_2c.setVisible(False)
    windows.b_3c.setVisible(False)
    windows.b_4c.setVisible(False)
    
    #Question 2
    windows.l_q2.setVisible(False)
    windows.l_q2.setStyleSheet(style_question)
    windows.b_oui.setVisible(False)
    windows.b_non.setVisible(False)
    
    #Question 3
    windows.l_q3.setVisible(False)
    windows.l_q3.setStyleSheet(style_question)
    windows.b_non_3.setVisible(False)
    windows.b_oui_3.setVisible(False)
    windows.b_alcool.setVisible(False)
    windows.b_aldehyde.setVisible(False)
    windows.b_acide.setVisible(False)
    
    #Question 4
    windows.l_q4.setVisible(False)
    windows.l_q4.setStyleSheet(style_question)
    windows.b_alcool_4.setVisible(False)
    windows.b_aldehyde_4.setVisible(False)
    windows.b_cetone_4.setVisible(False)
    windows.b_acide_4.setVisible(False)
    
    #Question 5
    windows.l_q5.setVisible(False)
    windows.l_q5.setStyleSheet(style_question)
    windows.b_cf1.setVisible(False)
    windows.b_cf2.setVisible(False)
    
    #Question 6
    windows.l_q6.setVisible(False)
    windows.l_q6.setStyleSheet(style_question)
    windows.b_c2.setVisible(False)
    windows.b_c3.setVisible(False)
    

def reset_boutons (n):
    """
    Fonction appelée "reset_boutons": réinitialise les styles et la visibilité des boutons à partir de l'indice `n`.

    Utilise les variables globales suivantes :
    - liste_boutons
    - liste_boutons_hide
    - style_bouton

    Fonctionnement :
    - Parcourt la liste des boutons (`liste_boutons`) à partir de l'indice `n`.
    - Réinitialise le style pour chaque bouton en utilisant `style_bouton`.
    - Parcourt la liste des indices de boutons masqués (`liste_boutons_hide`).
    - Rend le bouton correspondant invisible pour chaque indice à partir de `n`.
    
    """
    global liste_boutons
    global liste_boutons_hide
    global style_bouton
    debut = n-1
    for i in range(debut,len(liste_boutons)):
        bouton = liste_boutons[i]
        bouton.setStyleSheet(style_bouton)
        
    for i in range(liste_boutons_hide[debut], len(liste_boutons)):
        bouton = liste_boutons[i]
        bouton.setVisible(False)
        
        
def reset_labels (n):
    
    """
    Fonction "appui_bouton": masque les étiquettes à partir de l'indice `n`.

    Utilise la variable globale:
    - liste_labels

    Fonctionnement :
    - Parcourt la liste des étiquettes (`liste_labels`) à partir de l'indice `b`.
    - Rend chaque étiquette invisible.
    
    """
    global liste_labels
    debut = n-1
    windows.resultat.clear()

    for i in range(debut,len(liste_labels)):
        label = liste_labels[i]
        label.setVisible(False)
    
    
def appui_bouton(b):
    """
    Fonction appelée "appui_bouton": définit le style d'un bouton lorsqu'il est cliqué.

    (QPushButton): Le bouton qui a été cliqué.

    Exemple 
        appui_bouton(windows.b_c3) --> Applique le style spécifié au bouton C3.
        
    """
    
    style_bouton = """
        QPushButton {
        padding: 5px 5px;
        background-color: white;
        color: black;
        text-decoration: none;
        border: 2px solid #4e7cff; /* Cool blue border */
        border-radius: 5px;
        font-size: 15px;
        font-weight: bold;
        text-align: center;
    }
    """
    b.setStyleSheet(style_bouton)
        
        
def passer_au_q2(question):
    """
    Fonction appelée "passer_au_q2": affiche la deuxieme question (Q2) et les boutons "OUI" et "NON".
        
    """
    windows.l_q2.setText(question)
    windows.l_q2.setVisible(True)
    windows.b_oui.setVisible(True)
    windows.b_non.setVisible(True)


def passer_au_q3(question):
    """
    Fonction appelée "passer_au_q3": affiche la question du troisième niveau (Q3) et les boutons correspondants.

    """
    global reponse_q1
    
    windows.l_q3.setText(question)
    windows.l_q3.setVisible(True)
    
    if reponse_q1=="3C" or reponse_q1=="4C":
        windows.b_oui_3.setVisible(True)
        windows.b_non_3.setVisible(True)
        
    else:
        windows.b_alcool.setVisible(True)
        windows.b_aldehyde.setVisible(True)
        windows.b_acide.setVisible(True)
        
        
def passer_au_q4(question):
    """
    Fonction appelée "passer_au_q4": affiche la question du troisième niveau (Q4) et les boutons correspondants.

    """
    windows.l_q4.setText(question)
    windows.l_q4.setVisible(True)
    
    windows.b_alcool_4.setVisible(True)
    windows.b_aldehyde_4.setVisible(True)
    windows.b_cetone_4.setVisible(True)
    windows.b_acide_4.setVisible(True)
    
    
def passer_au_q5(question):
    """
    Fonction appelée "passer_au_q5": affiche la question du troisième niveau (Q5) et les boutons correspondants.

    """
    windows.l_q5.setText(question)
    windows.l_q5.setVisible(True)
    
    windows.b_cf1.setVisible(True)
    windows.b_cf2.setVisible(True)
    
    
def passer_au_q6(question):
    """
    Fonction appelée "passer_au_q6": affiche la question du troisième niveau (Q6) et les boutons correspondants.

    """
    windows.l_q6.setText(question)
    windows.l_q6.setVisible(True)
    
    windows.b_c2.setVisible(True)
    windows.b_c3.setVisible(True)
     
     
# ------------ ----------Boutons Questions 01 ----------------------
# Bouton 01
def b_1c_click():
    """
    Fonction appelée "b_1c_click" est appelée lorsque le bouton 1C est cliqué.

    - Définit la variable globale `reponse_q1` avec la valeur "1C".
    - Passe à la deuxième question (Q2) : "La molécule appartient-elle à une famille?"
    - Réinitialise les boutons (fonction `reset_boutons(1)`).
    - Réinitialise les étiquettes (fonction `reset_labels(2)`).
    - Applique le style au bouton 1C (fonction `appui_bouton(windows.b_1c)`).
    
    """
    global reponse_q1
    reponse_q1="1C"
    passer_au_q2("2) La molécule appartient-elle à une famille?")
    reset_boutons(1)
    reset_labels(2)
    appui_bouton(windows.b_1c)


# Bouton 02
def b_2c_click():
    global reponse_q1
    reponse_q1="2C"
    passer_au_q2("2) La molécule appartient-elle à une famille?")
    reset_boutons(1)
    reset_labels(2)
    appui_bouton(windows.b_2c)
    
    
# Bouton 03    
def b_3c_click():
    global reponse_q1
    reponse_q1="3C"
    passer_au_q2("2) La molécule porte-t-elle une ramification?")
    reset_boutons(1)
    reset_labels(2)
    appui_bouton(windows.b_3c)
    
    
# Bouton 04    
def b_4c_click():
    global reponse_q1
    global reponse_q2
    reponse_q1="4C"
    passer_au_q2("2) La molécule porte-t-elle une ramification?")
    reset_boutons(1)
    reset_labels(2)
    appui_bouton(windows.b_4c)
    
    
# ------------ ----------Boutons Questions 02 ----------------------   
# Bouton 05    
def b_oui_click():
    global reponse_q2
    reponse_q2="OUI"
    passer_au_q3("3) Choisir le nom de la famille")
    reset_boutons(5)
    reset_labels(3)
    appui_bouton(windows.b_oui)
    if reponse_q1=="1C" or reponse_q1=="2C":
        passer_au_q3("3) Choisir le nom de la famille: ")
    else:
        passer_au_q3("3) La molécule appartient-elle à une famille?")
    
    
# Bouton 06   
def b_non_click():
    global reponse_q1
    global reponse_q2
    reponse_q2="NON"
    reset_boutons(4)
    reset_labels(2)
    appui_bouton(windows.b_non)
    
    if reponse_q1=="1C" or reponse_q1=="2C":
        generer_resultat()
    else:
        passer_au_q3("3) La molécule appartient-elle à une famille?")
 
 
# ------------ ----------Boutons Questions 03 ----------------------
# Bouton 07
def b_alcool_click():
    global reponse_q3
    reponse_q3="Alcool"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_alcool)
    generer_resultat()
    
    
# Bouton 08 
def b_aldehyde_click():
    global reponse_q3
    reponse_q3="Aldehyde"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_aldehyde)
    generer_resultat()



# Bouton 9
def b_acide_click():
    global reponse_q3
    reponse_q3="Acide"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_acide)
    generer_resultat()
    
    
# Bouton 10
def b_oui_3_click():
    global reponse_q3
    reponse_q3="OUI"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_oui_3)
    passer_au_q4("4) Choisir le nom de la famille : ")


# Bouton 11
def b_non_3_click():
    global reponse_q3
    reponse_q3="NON"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_non_3)
    generer_resultat()


# ------------ ----------Boutons Questions 04 ----------------------    

# Bouton 12
def b_alcool_4_click():
    global reponse_q4
    reponse_q4="Alcool"
    reset_boutons(13)
    reset_labels(5)
    appui_bouton(windows.b_alcool_4)
    passer_au_q5("5) Quel est l'indice du carbone fonctionnel ?")


# Bouton 13
def b_aldehyde_4_click():
    global reponse_q4
    global reponse_q1
    global reponse_q2
    reponse_q4="Aldehyde"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_aldehyde_4)
    if reponse_q1=="4C" and reponse_q2=="OUI":
        passer_au_q6( "5)Quel est l'indice du carbone qui porte la ramification ?")
    else :
        generer_resultat()


# Bouton 14
def b_cetone_4_click():
    global reponse_q4
    global reponse_q2
    reponse_q4="Cetone"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_cetone_4)
    if reponse_q2=="NON" :
        passer_au_q5("5) Quel est l'indice du carbone fonctionnel ?")
    else :
        generer_resultat()


# Bouton 15
def b_acide_4_click():
    global reponse_q4
    global reponse_q2
    global reponse_q1
    reponse_q4="Acide"
    reset_boutons(7)
    reset_labels(4)
    appui_bouton(windows.b_acide_4)
    if reponse_q1=="4C" and reponse_q2=="OUI":
        passer_au_q6("5) Quel est l'indice du carbone qui porte la ramification ?")
    else :
        generer_resultat()


# ------------ ----------Boutons Questions 05 ----------------------  
# Bouton 16
def b_cf1_click():
    global reponse_q5
    global reponse_q2
    global reponse_q1
    reponse_q5="C1"
    reset_boutons(17)
    reset_labels(6)
    appui_bouton(windows.b_cf1)
    
    if reponse_q2=="NON" or (reponse_q1=="3C"):
        generer_resultat()
    else :
        passer_au_q6("6) Quel est l'indice du carbone qui porte la ramification ?")
        
        
# Bouton 17    
def b_cf2_click():
    global reponse_q5
    global reponse_q2
    global reponse_q1
    reponse_q5="C2"
    reset_boutons(17)
    reset_labels(6)
    appui_bouton(windows.b_cf2)
    
    if reponse_q2=="NON" or (reponse_q1=="3C"):
        generer_resultat()
    else :
        passer_au_q6("Quel est l'indice du carbone qui porte la ramification ?")
        
        

# ------------ ----------Boutons Questions 06 ----------------------  
# Bouton 18
def b_c2_click():
    global reponse_q6
    reponse_q6="C2"
    reset_boutons(19)
    appui_bouton(windows.b_c2)
    generer_resultat()
    
    
# Bouton 19   
def b_c3_click(): ##########
    """
    Fonction est appelée lorsque le bouton C3 (carbonne d'indice 3) est cliqué.

    Fonctionnement:
    -Définit la variable globale `reponse_q6`("Quel est l'indice du carbone qui porte la ramification ?")  avec la valeur "C3".
    -Réinitialise les boutons (fonction `reset_boutons(20)`).
    -Appuie sur le bouton `b_c3` de la fenêtre.
    -Génère le résultat (fonction `generer_resultat()`).

    Note : Assurez-vous d'avoir défini les fonctions `reset_boutons` et `generer_resultat`
    avant d'utiliser cette fonction.

    """
    global reponse_q6
    reponse_q6="C3"
    reset_boutons(20)
    appui_bouton(windows.b_c3)
    generer_resultat()


# Bouton Start
def b_start_click():
    """
    Fonction appelée lorsque le bouton "DEMARRER" est cliqué.

    Fonctionnement :
    - Masque l'étiquette "cours_label" (l'image du cours).
    #- Masque le bouton "b_try" (Let's try).
    - Masque le bouton "b_start" (Démarrer).
    - Masque l'étiquette "label_demarrer" (les instructions)
    - Affiche à nouveau l'étiquette "cours_label" (l'image du cours).
    - Affiche à nouveau le bouton "b_try" (Let's try).

    """
    #windows.b_try.setVisible(False)
    windows.cours_label.setVisible(False)
    windows.b_try.setVisible(False)
    windows.b_start.setVisible(False)
    windows.label_demarrer.setVisible(False)
    windows.cours_label.setVisible(True)
    windows.b_try.setVisible(True)
    
    
    
    
def b_try_click(): 
    """
    Fonction appelée lorsque le bouton "Let's go" est cliqué.

    Fonctionnement :
    - Affiche l'étiquette "label" (Acceuil).
    - Affiche le bouton "b_1C" (1 carbone).
    - Affiche le bouton "b_2C" (2 carbones).
    - Affiche le bouton "b_3C" (3 carbones).
    - Affiche le bouton "b_4C" (4 carbones).
    - Masque l'étiquette "cours_label" (l'image du cours).
    - Masque l'étiquette "b_try" (Let's try).

    """
    windows.label.setVisible(True)
    windows.b_1c.setVisible(True)
    windows.b_2c.setVisible(True)
    windows.b_3c.setVisible(True)
    windows.b_4c.setVisible(True)
    windows.cours_label.setVisible(False)
    windows.b_try.setVisible(False)
    

#------------------------------------------------------
app = QApplication([])
windows = loadUi ("chimieorganique.ui")
config_par_defaut()

windows.show()
windows.b_try.setVisible(False)
windows.b_start.clicked.connect ( b_start_click )

windows.b_try.clicked.connect ( b_try_click )

windows.b_1c.clicked.connect ( b_1c_click )
windows.b_2c.clicked.connect ( b_2c_click )
windows.b_4c.clicked.connect ( b_4c_click )
windows.b_3c.clicked.connect ( b_3c_click )

windows.b_non.clicked.connect ( b_non_click )
windows.b_oui.clicked.connect ( b_oui_click )

windows.b_alcool.clicked.connect(b_alcool_click)
windows.b_aldehyde.clicked.connect(b_aldehyde_click)
windows.b_acide.clicked.connect (b_acide_click)

windows.b_non_3.clicked.connect (b_non_3_click)
windows.b_oui_3.clicked.connect (b_oui_3_click)

windows.b_alcool_4.clicked.connect(b_alcool_4_click)
windows.b_aldehyde_4.clicked.connect(b_aldehyde_4_click)
windows.b_cetone_4.clicked.connect(b_cetone_4_click)
windows.b_acide_4.clicked.connect (b_acide_4_click)

windows.b_cf1.clicked.connect(b_cf1_click)
windows.b_cf2.clicked.connect (b_cf2_click)

windows.b_c2.clicked.connect(b_c2_click)
windows.b_c3.clicked.connect (b_c3_click)

liste_boutons = [windows.b_1c, windows.b_2c, windows.b_3c, windows.b_4c,
                 windows.b_oui, windows.b_non,
                 windows.b_alcool, windows.b_aldehyde, windows.b_acide, windows.b_oui_3, windows.b_non_3,
                 windows.b_alcool_4, windows.b_aldehyde_4, windows.b_cetone_4, windows.b_acide_4,
                 windows.b_cf1, windows.b_cf2,
                 windows.b_c2, windows.b_c3
                 ]

liste_labels = [windows.l_q2, windows.l_q3, windows.l_q4, windows.l_q5, windows.l_q6]
    

liste_boutons_hide = [6, 6, 6, 6,
              12, 12,
              16, 16, 16, 16, 16,
              18, 18, 18, 18,
              100, 100,
              100, 100]

liste_labels_hide = [2, 2, 2, 2,
              3, 3,
              4, 4, 4, 4, 4,
              5, 5, 5, 5,
              6, 6,
              7, 7]



app.exec_()




